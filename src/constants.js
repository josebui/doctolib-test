export const DAY_FORMAT = 'YYYY-MM-DD'
export const TIME_FORMAT = 'H:mm'
export const DEFAULT_DAYS_NUMBER = 7

export const EVENT_OPENING = 'opening'
export const EVENT_APPOINTMENT = 'appointment'

