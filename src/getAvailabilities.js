import moment from 'moment'
import knex from 'knexClient'
import _ from 'lodash'

import {
  DAY_FORMAT,
  TIME_FORMAT,
  DEFAULT_DAYS_NUMBER,
  EVENT_OPENING,
  EVENT_APPOINTMENT
} from './constants'

const getDayIndex = date => moment(date).format(DAY_FORMAT)

const getDays = (availabilities, indexes, date, event) => {
  const weekDay = date.format('d')
  const day = date.format(DAY_FORMAT)

  const foundIndexes = event.weekly_recurring
    ? _.uniq([
      ...(indexes.byDay[day] || []),
      ...(indexes.byWeekDay[weekDay] || [])
    ])
    : indexes.byDay[day] || []

  return foundIndexes.map(index =>
    availabilities[index]
  )
}

const updateSlots = (days, date) => {
  days.forEach(day =>
    day.slots.push(date.format(TIME_FORMAT))
  )
}

const removeSlots = (days, date) => {
  days.forEach(day =>
      day.slots = day.slots.filter(
        slot => slot.indexOf(date.format(TIME_FORMAT)) === -1
      )
  )
}

export default async function getAvailabilities(date, numberOfDays = DEFAULT_DAYS_NUMBER) {
  const availabilities = [...Array(numberOfDays).keys()]
    .map(i => moment(date).add(i, 'days').add(12, 'hour'))
    .map(tmpDate => ({
        date: tmpDate.toDate(),
        day: tmpDate.format(DAY_FORMAT),
        weekDay: tmpDate.format('d'),
        slots: []
    }))

  // Create indexes for quick search, by day and by week day
  const indexes = availabilities.reduce((indexes, availability, index) => {
    const { byDay, byWeekDay } = indexes
    const { day, weekDay } = availability
    return {
      byDay: {
        ...byDay,
        [day]: _.uniq([
          ...(byDay[day] || []),
          index
        ])
      },
      byWeekDay: {
        ...byWeekDay,
        [weekDay]: _.uniq([
          ...(byWeekDay[weekDay] || []),
          index
        ])
      }
    }
  }, {
    byDay: {},
    byWeekDay: {}
  })

  const events = await knex
    .select('kind', 'starts_at', 'ends_at', 'weekly_recurring')
    .from('events')
    .where(function() {
      this.where('weekly_recurring', true).orWhere('ends_at', '>', + date)
    })

  // Categorize events
  const {
    opening: openings = [],
    appointment: appointments = []
  } = _.groupBy(events, event => event.kind)

  // Add openings
  openings
    .forEach(event => {
      for (
        let date = moment(event.starts_at);
        date.isBefore(event.ends_at);
        date.add(30, 'minutes')
      ) {
        const days = getDays(availabilities, indexes, date, event)
        updateSlots(days, date)
      }
    })

  // Remove appointments
  appointments
    .forEach(event => {
      for (
        let date = moment(event.starts_at);
        date.isBefore(event.ends_at);
        date.add(30, 'minutes')
      ) {
        const days = getDays(availabilities, indexes, date, {})
        removeSlots(days, date)
      }
    })

  return availabilities
}
