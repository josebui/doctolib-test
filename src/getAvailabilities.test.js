import knex from 'knexClient'
import moment from 'moment'

import getAvailabilities from './getAvailabilities'
import { DAY_FORMAT, EVENT_OPENING, EVENT_APPOINTMENT } from './constants'

describe('getAvailabilities', () => {
  beforeEach(() => knex('events').truncate())

  describe('case 1', () => {
    it('test 1: Should have the number for expected days', async () => {
      const availabilities = await getAvailabilities(new Date('2019-08-10'))
      expect(availabilities.length).toBe(7)
      for (let i = 0; i < 7; ++i) {
        expect(availabilities[i].slots).toEqual([])
      }
    })
  })

  describe('case 2', () => {
    beforeEach(async () => {
      await knex('events').insert([
        {
          kind: EVENT_APPOINTMENT,
          starts_at: new Date('2019-08-11 10:30'),
          ends_at: new Date('2019-08-11 11:30')
        },
        {
          kind: EVENT_APPOINTMENT,
          starts_at: new Date('2019-08-11 12:00'),
          ends_at: new Date('2019-08-11 14:00')
        },
        {
          kind: EVENT_OPENING,
          starts_at: new Date('2019-08-04 09:30'),
          ends_at: new Date('2019-08-04 14:30'),
          weekly_recurring: true
        }
      ])
    })

    it('test 1', async () => {
      const availabilities = await getAvailabilities(new Date('2019-08-10'))
      expect(availabilities.length).toBe(7)

      expect(moment(availabilities[0].date).format(DAY_FORMAT)).toBe(
        '2019-08-10'
      )
      expect(availabilities[0].slots).toEqual([])

      expect(moment(availabilities[1].date).format(DAY_FORMAT)).toBe(
        '2019-08-11'
      )
      expect(availabilities[1].slots).toEqual([
        '9:30',
        '10:00',
        '11:30',
        '14:00'
      ])

      expect(moment(availabilities[6].date).format(DAY_FORMAT)).toBe(
        '2019-08-16'
      )
    })
  })

  describe('case 3', () => {
    beforeEach(async () => {
      await knex('events').insert([
        {
          kind: EVENT_APPOINTMENT,
          starts_at: new Date('2019-08-11 10:30'),
          ends_at: new Date('2019-08-11 11:30')
        },
        {
          kind: EVENT_OPENING,
          starts_at: new Date('2019-08-04 09:30'),
          ends_at: new Date('2019-08-04 12:30'),
          weekly_recurring: true
        }
      ])
    })

    it('test 1', async () => {
      const availabilities = await getAvailabilities(new Date('2019-08-10'))
      expect(availabilities.length).toBe(7)

      expect(moment(availabilities[0].date).format(DAY_FORMAT)).toBe(
        '2019-08-10'
      )
      expect(availabilities[0].slots).toEqual([])

      expect(moment(availabilities[1].date).format(DAY_FORMAT)).toBe(
        '2019-08-11'
      )
      expect(availabilities[6].slots).toEqual([])
    })
  })

  describe('Dynamic number of days', () => {
    beforeEach(async () => {
      await knex('events').insert([
        {
          kind: EVENT_APPOINTMENT,
          starts_at: new Date('2019-08-11 10:30'),
          ends_at: new Date('2019-08-11 11:30')
        },
        {
          kind: EVENT_OPENING,
          starts_at: new Date('2019-08-04 09:30'),
          ends_at: new Date('2019-08-04 12:30'),
          weekly_recurring: true
        },
        {
          kind: EVENT_OPENING,
          starts_at: new Date('2019-08-20 09:30'),
          ends_at: new Date('2019-08-20 11:30'),
          weekly_recurring: false
        }
      ])
    })

    it('Should display the expected number of days', async () => {
      const availabilities = await getAvailabilities(new Date('2019-08-10'), 10)
      expect(availabilities.length).toBe(10)
    })

    it('Should add openings and appointments properly', async () => {
      const availabilities = await getAvailabilities(new Date('2019-08-10'), 15)
      expect(moment(availabilities[14].date).format(DAY_FORMAT)).toBe(
        '2019-08-24'
      )
      expect(availabilities[0].slots).toEqual([])

      expect(moment(availabilities[1].date).format(DAY_FORMAT)).toBe(
        '2019-08-11'
      )
      expect(availabilities[6].slots).toEqual([])

      // Check not recurring working
      expect(moment(availabilities[3].date).format(DAY_FORMAT)).toBe(
        '2019-08-13'
      )
      expect(availabilities[3].slots).toEqual([])

      // Check one day working
      expect(moment(availabilities[10].date).format(DAY_FORMAT)).toBe(
        '2019-08-20'
      )
      expect(availabilities[10].slots).toEqual(['9:30', '10:00', '10:30', '11:00'])
    })
  })
})
